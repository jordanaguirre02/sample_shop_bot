from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment

def create_new_order_sheet(path):
    #Create new order spread sheet
    workbook = Workbook()
    sheet = workbook.active
    #premake all potential rows and columns in the order sheet
    for num in range(1, 118):
        #Create cell sizes
        sheet.merge_cells(f"A{num}:E{num}")
        sheet.merge_cells(f"F{num}:H{num}")
        sheet.merge_cells(f"I{num}:J{num}")
        sheet.merge_cells(f"K{num}:L{num}")
        sheet.merge_cells(f"M{num}:P{num}")
        sheet.merge_cells(f"Q{num}:Z{num}")
        #Define cell names, empty cell to patch bug on merge cells
        name_cell = sheet[f"A{num}"]
        set_and_card_number_cell = sheet[f"F{num}"]
        quantity_cell = sheet[f"I{num}"]
        price_cell = sheet[f"K{num}"]
        est_total_cell = sheet[f"M{num}"]
        empty_cells = sheet[f"Q{num}"]
        #Align all cells
        name_cell.alignment = Alignment(
            horizontal="center",
            vertical="center"
        )
        set_and_card_number_cell.alignment = Alignment(
            horizontal="center",
            vertical="center"
        )
        quantity_cell.alignment = Alignment(
            horizontal="center",
            vertical="center"
        )
        price_cell.alignment = Alignment(
            horizontal="center",
            vertical="center"
        )
        est_total_cell.alignment = Alignment(
            horizontal="center",
            vertical="center"
        )
        #Give top cells their cell names
        if num == 1:
            sheet[f"A{num}"] = "Card Name"
            sheet[f"F{num}"] = "Set/Card Number"
            sheet[f"I{num}"] = "Quantity"
            sheet[f"K{num}"] = "Card Price"
            sheet[f"M{num}"] = "Estimated Card(s) Total"
            sheet[f"Q{num}"] = ""
        #Save spread sheet
        workbook.save(path)

#Edit order sheet with card data
def edit_order_sheet(path, data):
    workbook = load_workbook(filename=path)
    order_sheet = workbook.active
    #data is a dictionary, loops through data to add to spread sheet
    for cell in data.keys():
        order_sheet[cell] = data[cell]
    #save spread sheet
    workbook.save(path)
