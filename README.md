# Sample Shop Discord Bot

## Steps:
1. Create virtual environment
    - ```python -m venv .venv```
2. Activate virtual environment
    - ```.venv/scripts/activate.ps1```   (Windows)
    - ```source ./.venv/bin/activate```   (Mac)
3. Install requirements
    - ```pip install -r requirements.txt```
4. Create New Discord bot application via
    - https://discord.com/developers/applications
    - ![alt text](readme_images/steps/NewApp.png)
5. Set Bot permissions
    - ![alt text](readme_images/steps/SetPermissions.png)
    - ![alt text](readme_images/steps/BotPermissions.png)
6. Set Bot intents and create BOT_TOKEN (save temporarily)
    - ![alt text](readme_images/steps/Intents.png)
    - ![alt text](readme_images/steps/BotToken.png)
7. Generate Bot invite link
    - ![alt text](readme_images/steps/InviteLink.png)
    - ![alt text](readme_images/steps/Scope.png)
    - ![alt text](readme_images/steps/BotPermissions.png)
    - ![alt text](readme_images/steps/GeneratedLink.png)
8. Invite Bot to server
    - ![alt text](readme_images/steps/InviteToServer.png)
9. In .venv/Lib/site-packages folder create Keys folder and keys.py file
    - ![alt text](readme_images/steps/FolderPath.png)
    - ![alt text](readme_images/steps/KeysFolder.png)
10. Add BOT_TOKEN variable to keys.py file
    - ![alt text](readme_images/steps/BotTokenInKeys.png)

## As User:
1. Find or add list on Mox field
    - ![alt text](readme_images/user/FromMox.png)
2. Export and copy Deck list
    - ![alt text](readme_images/user/ExportDeck.png)
    - ![alt text](readme_images/user/CopyList.png)
3. Inital Shop command
    - ![alt text](readme_images/user/ShopCommand.png)
4. In private message, use buy command with deck list
    - ![alt text](readme_images/user/GoToDM.png)
    - ![alt text](readme_images/user/PasteCardList.png)
5. Await bot response
    - ![alt text](readme_images/user/BotBuyResponse.png)
6. Order Sheet is created
    - ![alt text](readme_images/user/ViewOrderList.png)
