#List of responses to organization purposes

shop_response = """Thank you for shopping with us today. Type /buy followed by
your moxfield card list to get started"""

buy_response = """A team member has been notified and will get back to you
with your order availability shortly"""

error_response = """There has been an error in the submitted deck list,
please make sure the list was copied directly from Moxfield and try again."""
