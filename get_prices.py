import requests
from shop_order import create_new_order_sheet, edit_order_sheet
from time import sleep


def strip_card(card):
    #Split card string into a list of parts, pulls our relevant data
    card_mess = card.split(" ")
    quantity = card_mess.pop(0)
    card_number = card_mess.pop(-1)
    card_set = card_mess.pop(-1)
    #Reorganizes list to just contain the card name
    card_name_list = [card_name_part for card_name_part in card_mess if "(" not in card_name_part]
    card_name = " ".join(card_name_list)
    #Turns into a query to be used on various APIs/ web look ups
    card_query = card_name.replace(" ", "+")
    card_query = card_query.replace("'", "%27")
    #return all relevant data
    return card_query, quantity, card_number, card_set

def card_look_up(card_query):
    #References Scryfall API to pull exact card name and sample price
    scryfall_url = f"https://api.scryfall.com/cards/named?fuzzy={card_query}"
    response = requests.get(scryfall_url)
    card_name = response.json()["name"]
    card_price = response.json()["prices"]["usd"]
    #returns card name and sa,ple card price
    return card_name, card_price

def make_card_list(cards_string):
    #turns the moxfield list into a workable list
    cards = cards_string.split("\n")
    return cards

def card_check(cards_string, message):
    #Error handling with a checked variable that only changes if all functions are successful
    checked = False
    est_list_total = 0
    #Generate Order sheet name with user's discord name
    order_sheet_path = f"./shop_order/{message.author}_order.xlsx"
    #Create the initial spread sheet
    create_new_order_sheet(order_sheet_path)
    cards = make_card_list(cards_string)
    #Loops through all cards in the list and adds relevant data to order sheet
    for index, card in enumerate(cards):
        #index plus 2, as index starts at 0 and first cell is for table names
        sequence = index + 2
        card_query, quantity, card_number, card_set = strip_card(card)
        card_name, card_price = card_look_up(card_query)
        #Tally up card list total throughout the loop
        est_card_total = int(quantity) * float(card_price)
        est_list_total += est_card_total
        #Creates the data to send to the order sheet for each card
        data = {
            f"A{sequence}": card_name,
            f"F{sequence}": f"{card_set}/{card_number}",
            f"I{sequence}": quantity,
            f"K{sequence}": card_price,
            f"M{sequence}": est_card_total,
            f"Q{sequence}": ""
        }
        edit_order_sheet(order_sheet_path, data)
    #Create final total data for order sheet after loop is complete
    data = {
        f"K{sequence + 1}": "Grand Total:",
        f"M{sequence + 1}": est_list_total,
        f"Q{sequence + 1}": ""
    }
    edit_order_sheet(order_sheet_path, data)
    #Indicate all functions have passed and return the result
    checked = True
    return checked
