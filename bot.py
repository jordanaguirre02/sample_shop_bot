import discord
from discord.ext import commands
from shop_commands import shop_commands
from Keys import keys

def run_bot():
    #Set intents to allow bot to read messages
    intents = discord.Intents.default()
    intents.message_content = True
    bot = commands.Bot(command_prefix="!", intents=intents, help_command=None)

    @bot.event
    async def on_ready():
        #To let you know the bot is running and some motivation
        print("this works congrats now make a good bot.")

    @bot.event
    async def on_message(message):
        #Prevent infinite loops from bot output messages
        if message.author == bot.user:
            return
        #first check to make sure a slash command was inniated
        if "/" == message.content[0]:
            #Send to the shop commands to further filter
            await shop_commands(message)
    #runs the bot
    bot.run(keys.BOT_TOKEN)
