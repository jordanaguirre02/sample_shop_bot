import responses
import discord
from get_prices import card_check

async def shop_commands(message):
    #Convert message to uniform lowercase and check if it is the shop command
    if message.content.lower() == "/shop":
        #Simple error handling
        try:
            #Send private message to further assist customer
            await message.author.send(responses.shop_response)
        except Exception as e:
            print(e)
    #Checks that buy command is in a private message with bot and uniform lowercase
    if isinstance(message.channel, discord.DMChannel) and message.content[:4].lower() == "/buy":
        #Strips the command from the message leaving just the moxfield list
        mox_list = message.content[5:].lower()
        #Send card list and message data to the card check function
        checked = card_check(mox_list, message)
        #If returned as successful the private message customer letting them know order is being pulled
        if checked:
            try:
                await message.author.send(responses.buy_response)
            except Exception as e:
                print(e)
        else:
            #Send error message if an error occured
            await message.author.send(responses.error_response)
